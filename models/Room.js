const mongoose = require('mongoose')
const { Schema } = mongoose
const roomchema = mongoose.Schema({
  name: String,
  capacity: { type: Number, default: 50 },
  floor: Number,
  building: [{ type: Schema.Types.ObjectId, ref: 'building' }]
})

module.exports = mongoose.model('Room', roomchema)
